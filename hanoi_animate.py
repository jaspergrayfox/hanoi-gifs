from glob import glob

from PIL import Image

from hanoi_frame import gen_frame
from hanoi_steps import gen_states

def animate(level, from_peg=0, to_peg=2):
    states = gen_states(level, from_peg, to_peg)
    frames = []
    for i, state in enumerate(states):
        frames.append(gen_frame(state, level, None))
    return frames

def create_gif(level, frames, filename):
    frames[0].save(filename, 
        format="GIF",
        save_all=True,
        append_images=frames[1:],
        duration=max(5, int(5000.0 / len(frames))),
        loop=0)
    for frame in frames:
        frame.close()

if __name__ == "__main__":
    for level in range(1, 13):
        frames = animate(level)
        create_gif(level, frames, f"hanoi_steps_{level}.gif")
