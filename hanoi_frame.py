from PIL import Image, ImageDraw, ImageColor

BLOCK_UNIT = 16
MAX_DISKS = 21
MAX_DISK_WIDTH = 10 # in block units

def draw_floor(d):
    d.rectangle((BLOCK_UNIT,
                 (MAX_DISKS + 2) * BLOCK_UNIT,
                 (MAX_DISK_WIDTH * 3 + 5) * BLOCK_UNIT,
                 (MAX_DISKS + 3) * BLOCK_UNIT),
                fill="gray")

def get_disk_colour(disk, level):
    hue = 360 / level * (disk - 1)
    return ImageColor.getrgb(f"hsv({hue}, 100%, 100%)")
    
def draw_peg(d, level, i, peg_contents):
    '''
    Draw the `i`-th peg using the drawing context `d`.
    The peg has contents `peg_contents` and the figure represents
    a state for a hanoi puzzle with `level` levels.
    '''
    # The middle x value of the peg
    x_offset = (2 + MAX_DISK_WIDTH * i + MAX_DISK_WIDTH / 2) * BLOCK_UNIT
    d.rectangle((x_offset - 1, BLOCK_UNIT,
                 x_offset + 1, (MAX_DISKS + 2) * BLOCK_UNIT),
                fill="gray")
    y_offset = 23 * BLOCK_UNIT
    for disk in peg_contents:
        y_offset -= BLOCK_UNIT
        disk_width = ((float(disk) + MAX_DISKS - level) / MAX_DISKS) * 10 * BLOCK_UNIT
        d.rectangle((x_offset - disk_width / 2, y_offset,
                     x_offset + disk_width / 2, y_offset + BLOCK_UNIT - 1),
                    fill=get_disk_colour(disk, level))

# Example input
# [[2, 3], [1], []]
def gen_frame(state, level, filename=None):
    assert len(state) == 3
    assert level <= MAX_DISKS
    out = Image.new("RGB", 
                    (36 * BLOCK_UNIT, (MAX_DISKS + 4) * BLOCK_UNIT),
                    "white")
    d = ImageDraw.Draw(out)
    draw_floor(d)
    for i in range(3):
        draw_peg(d, level, i, state[i])
    if filename is not None:
        out.save(filename, "png")
    return out

if __name__ == "__main__":
    gen_frame([[4, 3], [1], [2]], 4, "hanoi_frame_test.png")
    gen_frame([[], [], [3, 2, 1]], 3, "hanoi_test_2.png")

