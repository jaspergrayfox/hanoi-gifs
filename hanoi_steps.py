from copy import deepcopy

# Example:
# For level = 1, [(0, 2)] to indicate going from 0th peg to 2nd
# For level = 2, [(0, 1), (0, 2), (1, 2)]
def gen_steps(level, from_peg=0, to_peg=2):
    if level == 0:
        return []
    unused_peg = 0
    for i in range(3):
        if from_peg != i and to_peg != i:
            unused_peg = i
            break
    first_half = gen_steps(level - 1, from_peg, unused_peg)
    bottom_plate_move = (from_peg, to_peg)
    second_half = gen_steps(level - 1, unused_peg, to_peg)
    return first_half + [bottom_plate_move] + second_half

# Example:
# For level = 1, [[[1], [], []], [[], [], [1]]]
# The innermost arrays are more like stacks
def gen_states(level, from_peg=0, to_peg=2):
    initial_state = [[], [], []]
    for i in range(level):
        initial_state[0].append(level - i)

    steps = gen_steps(level, from_peg, to_peg)

    states = []
    current_state = initial_state
    states.append(deepcopy(current_state))
    for (from_peg, to_peg) in steps:
        current_state[to_peg].append(current_state[from_peg].pop())
        states.append(deepcopy(current_state))
    return states

if __name__ == "__main__":
    assert gen_steps(1, 0, 2) == [(0, 2)]
    assert gen_steps(2, 0, 2) == [(0, 1), (0, 2), (1, 2)]
    assert gen_steps(2, 1, 0) == [(1, 2), (1, 0), (2, 0)]
    print(gen_states(3))
