# Hanoi GIFs

This repository creates animated GIFs of solutions to the
Tower of Hanoi puzzle. It is written in Python and uses the Pillow library.

## Setup

To set up the project, run the following commands in a terminal:

```sh
git clone https://gitlab.com/jaspergrayfox/hanoi-gifs.git
cd hanoi-gifs
python -m venv .venv
source .venv/bin/activate
python -m pip install -r requirements.txt
python hanoi_animate.py
```

By default, the Python file creates 12 GIFs, with each GIF being an animated
solution of a Tower of Hanoi puzzle with n disks, where n is an integer
from 1 to 12.
